;set is the generic interface for the implementation of sets. It is a function that takes the 
;implementing class's constructor, it's type, and several functions that implement the implementing
;set's core behaviors as arguments. Note that this generic set constructor should never be called
;outside of an implementing class constructor, since that would destroy the ability to add 
;new actions not supported by the generic set and actions would have to be called in a way that is 
;incompatible with implementing subclasses. Because of this, set is more of an interface than a class.
;It provides implementations for generic functions and a general guideline for the implementation of 
;a set using the arguments that it takes as parameters. 
;Preconditions:
;builder is a reference to the constructor function that is calling this generic set constructor.
;type is an atom that represents the type of set that the implementing class is. This is only an 
;     identifier and is not not used inside the generic interface.
;containsf is a function that takes an element as a paramater and returns #t if the element exists in
;     the set represented by the implementing class, false if not. 
;tosortedlistf is a function that takes no arguments and returns the set's data in the form of a
;     sorted list (elements in nondecreasing order)
;tolistf is a function that takes no arguments and returns the set's data in the form of a list that 
;     is not necessarily sorted.
;insertf is a function that takes an element as an argument and inserts it into the set. It does not
;     return anything. 
;removef is a function that takes an element as an argument and removes it from the set if it exists.
;     Nothing is returned.
;sizef is a function that takes no arguments and returns the number of elements in the set.
;Postconditions: a dispatching function is returned through which a number of actions can be 
;    executed. It is recommended that all subclasses have their own dispatcher in order to have the 
;    ability to add actions not defined by the general interface. If a requested action does not 
;    exist in the subclass's dispatcher, the general set's dispatcher should be used.

(define set
  (lambda (builder type containsf tosortedlistf tolistf insertf removef sizef)
    
    (define (perform-on-list lst action)
      (cond ((not (null? lst)) 
             (begin (this action (car lst))
               (perform-on-list (cdr lst) action)))))
    ;Precondition: action is a valid route supported by the dispatching function "this", otherwise 
    ;nothing happens.
    ;Postcondition: The action has been performed on every element in the list. The empty list is 
    ;returned. 
    ;Role in Program: High order function that iterates through in input list and dispatches the 
    ;action, passing as a parameter each of the list elements one by one. 
    ;Example: (perform-on-list '(1 2 3) 'insert) will insert the elements 1, 2, and 3 into this set.
    ;Proof: 
    ;   Base Case: If lst is null, the recursion stops and the empty list (lst) is returned.  
    ;   Inductive Hypothesis: Assuming (perform-on-list list action) works correctly for a list of 
    ;   size k.
    ;   Inductive Step: According to Inductive Hypothesis, since (perform-on-list list action) works 
    ;     on k elements, it should also work for k+1 elements. So the action that will be perform on 
    ;     is [(perform-on-list list action) on K+1] is the same as [(perform-on-list list action) on K] + 
    ;     [(perform-on-list list action) on (one extra element)] so this program will work correctly
    ;Termination: After the first call of (perform-on-list list action), as long as the list is not empty,
    ;     we call (perform-on-list (cdr lst) action) to loop through another time. each time this is call, 
    ;     we have one element taken off lst. Since we have finite number of elements in the list, eventually
    ;     the list will become null and the loop terminates.
    
    (define (true-for-all pred? lst)
      (cond ((null? lst) #t)
            ((pred? (car lst)) (true-for-all pred? (cdr lst)))
            (else #f)))
    ;Precondition: Assum the input condition is a predicate and proper list
    ;Postcondition: returns true if every element in the list satistfy the predicate, and false if one doesn't
    ;Role in Program: It is a helper function that helps other function to determine whether all elements in
    ; list statistfy the condition of the predicate.
    ;Proof:
    ;    Base Case: If the list is null then the program stops and also if one element in the list evaluated
    ;       to false, then the program will also reach termination.
    ;    Inductive Hypothesis: Assume (true-for-all pred? lst) correctly for list of size k.
    ;    Inductive Step: Base on the inductive hypothesis, since this program work correctly for list of size k
    ;       , then it should work correctly for size of k+1 element. (true-for-all pred? lst) evalute to true
    ;       when #t1 AND #t2 AND .... #tk. If (true-for-all pred? lst) of size k evaluate to false, we know 
    ;       that the additional element that will be added will be false, beacuse [#f(1-k)] AND [#t or #f] 
    ;       will evaluted to false. and when (true-for-all pred? lst) of size k evaluate to true, the 
    ;       (pred? (car lst)) of the last element will determine whether to return true or false. Because 
    ;       [#t(1-k)] AND [X] = [X] where X = #t or #f.
    ;Termination: The program will call (true-for-all pred? (cdr lst)) as long as lst is not an empty list 
    ; and no predicate evaluates to false. (true-for-all pred? (cdr lst)) will take one element out each 
    ; time it was called. eventually, either we will evaluate a predicate to be false or lst is a empty list
    ; then termination occur.
    
    ;create new set with elements of this set for which pred? is true
    (define (filter pred? lst)
      (let ((temp-set (builder)))   
        (define (aux-filter lst)
          (cond ((null? lst) temp-set)
                ((pred? (car lst)) (begin (temp-set 'insert (car lst)) (aux-filter (cdr lst))))
                (else (aux-filter (cdr lst)))))
        (aux-filter lst)))
    ;Precondition: First argument has to be a predicate, the second argument has to be a list. insertf
    ;and builder must work correctly
    ;Postcondition: Returns a new set with elements of lst for which pred? is true;
    ;Role in the Program: Used to implement the 'filter, 'intersection, and 'difference actions
    ;Proof:
    ;   Base Case: when the lst is null, temp-set is returned without further modifications. If lst 
    ;      was originally null, that makes temp-set an empty set, making this correct.
    ;   Inductive Hypothesis: assume (filter pred? lst) works correctly with input size k.
    ;   Inductive Step: According to the inductive hypothesis, since (filter pred? lst) works for input of 
    ;      size k, then it should work for input of size k+1. In the function, temp-set is a temporary set
    ;      that is used to store the elements that meets the criteria of pred?. After we got thorugh list of
    ;      element of size k, we obtain a temp-set that has elements that met pred?. and we have one more 
    ;      element in the list, if that element met the pred? then it would be included in temp-set, but if
    ;      it does not, then we would still have the same list from the k-th element.
    ;Termination: each time (aux-filter (cdr lst)) is called, one element in the front of the list is 
    ;      removed. At some point, the list will be null, the base case will be met, and the function 
    ;      will terminate.   
    
    (define (make-same-type-from-list lst) (filter (lambda (e) #t) lst))
    ;Precondition: lst is a list of numbers. builder, the constructor for the kind of set currently 
    ;inheriting this function, correctly returns a new set of the type 'type. The action 
    ;'insert-multiple correctly inserts all elements in lst into the set object from which it is 
    ;invoked. 
    ;Postcondition: A new set of type 'type containing all the elements in list has been returned
    ;Proof: As long as the assumptions in the precondition hold, the function trivially works 
    ;because it relies only on the execution of builder and set's action 'insert-multiple.
    
    (define (this req . args)
      (cond ((eq? req 'type) type)
            ((eq? req 'insert-multiple) (perform-on-list (car args) 'insert))
            ((eq? req 'remove-multiple) (perform-on-list (car args) 'remove))
            ((eq? req 'insert) (insertf (car args)))
            ((eq? req 'remove) (removef (car args)))
            ((eq? req 'empty?) (= (sizef) 0))
            ((eq? req 'size) (sizef))
            ((eq? req 'print) (begin (display (tolistf)) (write-char #\newline)))
            ((eq? req 'tosortedlist) (tosortedlistf))
            ((eq? req 'tolist) (tolistf))
            ((eq? req 'contains) (containsf (car args)))
            ((eq? req 'copy) (make-same-type-from-list (tolistf)))
            ((eq? req 'filter) (filter (car args) (tolistf)))
            ((eq? req 'equals?) (equal? (tosortedlistf) ((car args) 'tosortedlist)))
            ((eq? req 'union) 
             (make-same-type-from-list 
               (append (tolistf) ((car args) 'tolist))))
            ((eq? req 'is-superset-of?)
             (true-for-all containsf((car args) 'tolist)))
            ((eq? req 'is-subset-of?)
             ((car args) 'is-superset-of? this))
            ((eq? req 'intersection) 
             (this 'filter (lambda (e) ((car args) 'contains e))))
            ((eq? req 'difference) 
             (this 'filter (lambda (e) (not ((car args) 'contains e)))))
            (else 'bad-request)) ) 
    this))


;Constructor for tree-based set. Takes no arguments, returns a dispatching function that simply calls
; the generic interface's dispatcher with the arguments passed in, since we have not added new types of 
; actions.
;This tree based set is backed by a binary search tree stored in an instance variable named "data". 
;Each node of this tree is a three-element list of the following form:
;    '(left_structure      root      right_structure).
;The following functions are implemented: tree-based-contains, tree-to-list, tree-based-insert,
; tree-based-remove, tree-size, and tree-depth. It should be noted that tree-depth will only work with
; sets that have trees as their structure.
(define (treeset)
  
  (define data '()) 
  
  (define (root-of-tree t)
    (cadr t))
  ;Preconditions: The input must be a list of three items which represents a tree-based set with the
  ; form '(left_structure       root      right_structure). 
  ;Postconditions: This function returns the root of the tree that is the input.
  ;Role in Program: This function is used to get the root of any non-empty tree in this program which can
  ; be used for traversing the tree. 
  ;Proof: Assuming the “cadr” works correctly and the input is correct, in other words a list having the form
  ; '(left_structure      root      right_structure), this function will return the second element of the
  ; input which will be the root.
  
  (define (left-tree t)
    (car t))
  ;Preconditions: The input must be a list of three items which represents a tree-based set with the form
  ; '(left_structure      root      right_structure). 
  ;Postconditions: This function returns the left structure of the tree that is the input. The
  ; left structure that is returned could be either null, or a subtree.
  ;Role in Program: This function is used in traversing trees in this program.
  ;Proof: Assuming the “car” works correctly and the input is correct, this function will
  ; return the first element of the input which will be the left structure.
  
  
  (define (right-tree t)
    (caddr t))
  ;Preconditions: The input must be a list of three items which represents a tree-based set with
  ; the form '(left_structure      root      right_structure). 
  ;Postconditions: This function returns the right structure of the tree that is the input.
  ; The right structure that is returned could be either null, or a subtree.
  ;Role in Program: This function is used in traversing trees in this program.
  ;Proof: Assuming the “caddr” works correctly and the input is correct, this function will return
  ; the third element of the input which will be the right structure.
  
  (define (tree-leaf? tree)
    (and (null? (left-tree tree)) (null? (right-tree tree))))
  ;Preconditions: The input must be a list of three items which represents a tree-based set with
  ; the form '(left_structure      root      right_structure).
  ;Postconditions: This function returns True if the argument is a leaf and False otherwise.
  ;Role in Program: This function is used in traversing trees in this program.
  ;Proof: A tree node is considered a leaf when it has no left or right children. Since this
  ; function returns true when the left and right structures are both null, this function works
  ; correctly.
  
  (define (left-tree-null? tree)
    (null? (left-tree tree)))
  ;Preconditions: The input must be a list of three items which represents a tree-based set with
  ; the form '(left_structure      root      right_structure).
  ;Postconditions: This function returns True if the argument has no left child. Otherwise it
  ; returns False.
  ;Role in Program: This function is used in traversing trees in this program.
  ;Proof: If "left-tree" works correctly, this function will work correctly. 
  
  
  (define (right-tree-null? tree)
    (null? (right-tree tree)))
  ;Preconditions: The input must be a list of three items which represents a tree-based set with
  ; the form '(left_structure      root      right_structure).
  ;Postconditions: This function returns True if the argument has no right child. Otherwise it
  ; returns False.
  ;Role in Program: This function is used in traversing trees in this program.
  ;Proof: If "right-tree" works correctly, this function will work correctly. 
  
  (define (tree-based-contains target)
    (define (tree-based-contains-helper tset target)
      (cond ((null? tset) #f)
            ((= target (root-of-tree tset)) #t)
            ((tree-leaf? tset) #f)
            ((< target (root-of-tree tset)) (if (left-tree-null? tset) #f 
                                                (tree-based-contains-helper (left-tree tset) target)))
            ((> target (root-of-tree tset)) (if (right-tree-null? tset) #f 
                                                (tree-based-contains-helper (right-tree tset) target)))))
    (tree-based-contains-helper data target))
  ;Preconditions: The input must be a number, "target"
  ;Postconditions: This function returns True if the "target" argument is a member of
  ; the treeset's "data" instance variable.
  ;Role in Program : This function is used to check whether an element is in a general set
  ; if the underlying data-structure is a tree.
  ;Proof: Induction on number of elements, "k", in the section of the tree
  ; still to be traversed.
  ; Base Case - If the input is null, then k is 0 so the target cannot be in the input set
  ; Wish - "tree-based-contains tset[k-n]" correctly returns True if target is in tset[k-n]
  ; and false otherwise where tset[k-n] is a subset of tset with "n" elements removed.
  ; Inductive step - If the target is equal to the root value, the function returns True because
  ; target has been found in the set. 
  ; If the target is less than the value of the root, the right
  ; child is ignored because the target cannot be in the right child. The function checks whether
  ; the left tree is null. If it is, then k is 0 and the target cannot exist in the set. If the
  ; left tree is not null, the function returns (tree-based-contains tset[k-n]) where n is the
  ; number of elements in the right child. 
  ; If the target is greater than the value of the root, the program does a similar procedure
  ; as when the target was less than the value of the root but using the right tree this time.
  
  
  (define (tree-size)
    (define (tree-size-helper tree)
      (cond ((null? tree) 0)
            ((tree-leaf? tree) 1)
            (else (+ 1 (+ (tree-size-helper (left-tree tree)) 
                          (tree-size-helper (right-tree tree)))))))
    (tree-size-helper data))
  ;Preconditions: This function takes an instance variable "data" as its input and does not need
  ; any explicit inputs.
  ;Postconditions: This function returns the number of elements in the tree "data".
  ;Role in Program: This function is used to give the size of a generic set when a tree is the
  ; underlying data structure.
  ;Proof: induct on the depth of the unprocessed tree - "k"
  ; Base case - If the input is null, k must be 0 and the tree has 0 elements so the function 
  ; returns 0. If the input is a leaf, "k" is 1 at the beginning of execution and is 0 at the end
  ; and the set must have 1 element so the function returns 1.
  ; Assume - The function correctly counts a tree of depth k-1.
  ; Inductive step - If given a tree of depth k, the function adds 1 to the sum of the sizes of its
  ; left and right sub-trees. With the current node removed and accounted for, the sum of the elements
  ; in the subtrees is calculated using recursive calls to tree-size on trees with depths k-1.
  
  
  (define (list-to-tree l)
    ;Find the middle count of the tree
    (define (middle l) (floor (/ (length l) 2)))
    ;Precondition: The input must be a list
    ;Postcondition: This function returns the index of element in the middle of "l"
    ;Role in Program: The index of the middle element is used in building a tree from a list. The
    ; middle element is turned into the root of a tree and the other elements are then inserted
    ; into the new tree.
    ;Proof: If "floor" and "length" work correctly, this function will work correctly.
    
    ;Find the element in the middle
    (define (itr-to-mid count l)
      (cond ((= count 0) (car l))
            (else (itr-to-mid (- count 1) (cdr l)))))
    ;Precondition: Input must be a positive integer, "count", and a list, "l".
    ;Postcondition: This function returns the "count"th element in "l"
    ;Role in Program: This function is used with the "middle" function to get the middle element of a
    ; list when converting it into a tree.
    ;Proof: Design roles - "count" is the distance of the desired element from the front of the list.
    ; Begin - At the beginning of execution, the desired element must be "count" elements away from
    ; the front since this is how the function is called.
    ; Execution - With each execution, a recursive call is made with "count" reduced by one and the cdr
    ; of the list. The desired element is one element closer to the front of the list and "count"
    ; is reduced by 1. Thus, the design role is maintained
    ; Termination - The program terminates when count reaches 0 and since "count" is a positive integer
    ; and is being reduced by 1 with each iteration, it must reach 0 eventually.
    
    ;using tree insert to insert each element into the tree
    (define (treeify-aux init l)
      (cond ((null? l) init)
            (else (treeify-aux (tree-based-insert-helper init (car l)) (cdr l)))))
    ;Precondition: "init" must be a leaf or a tree and "l" must be a list whose elements work with
    ; tree-based-insert-helper.
    ;Postcondition: This function returns a tree that contains the elements of "l" and whose root
    ; is "init" if init was a leaf. 
    ;Role in Program: This function is the main component of the list-to-tree function.
    ;Proof: Design Roles - "l" contains the elements still to be inserted.
    ; Begin - The function is called with "l" being the elements to be inserted.
    ; Execution - With each iteration, a recursive call is made with the car of "l" added to "init"
    ; and with the new "l" as the cdr of the old "l". Thus the element that was inserted into "init"
    ; is removed from "l" and the design role is maintained.
    ; Termination - Execution ends when "l" is empty. Since with each iteration an element is removed
    ; from "l" nothing is added to "l", "l" will eventually be empty.
    
    (if (null? l) l 
        (treeify-aux (tree-based-insert-helper '() (itr-to-mid (middle l) l)) l))
    )
  ;Precondition: The input must be a list, "l", of elements that work with tree-based-insert-helper.
  ;Postcondition: The output will be a tree that contains the elements of "l".
  ;Role in Program: This function is used when performing operations on a general set whose data
  ; structure is a tree. The tree is first converted to a list, operated on as a list, and the result
  ; is then converted back into a tree. 
  ;Proof: If treeify-aux, tree-based-insert-helper, iter-to-mid, and middle work correctly, this
  ; function will work correctly. It creates the root of the tree by creating a list with the middle
  ; element in "l" and then inserts the other elements of "l" into the tree.
  
  
  (define (tree-based-insert-helper tset e)
    (define (insert-at-leaf leaf e)
      ; return leaf (now node) with pointer to new element
      (cond ((< e (root-of-tree leaf)) (list (list '() e '())
                                             (root-of-tree leaf)
                                             '()))
            ((= e (root-of-tree leaf)) leaf)
            ((> e (root-of-tree leaf)) (list '()
                                             (root-of-tree leaf)
                                             (list '() e '()))))
      )
    ;Preconditions: The input must be a list of three elements that represents a tree leaf.
    ; It should be of the form '(null      root      null), "leaf", and a number "e"
    ;Postconditions: This function will return a tree node whose root is the leaf that was
    ; the argument and whose child is "e" if "e" is not equal to the value of the root.
    ;Role in Program: This function is an auxiliary function to the "tree-based-insert" function.
    ;Proof: Assuming root-of-tree works correctly, this function will work correctly. If "e" is
    ; equal to the value of the root, it can be seen from the code that the function will return 
    ; the leaf that was inserted because a set should not have duplicates. 
    ; If "e" is greater than the root, the function returns a tree node whose left child is null,
    ; root is the root of the leaf argument, and right child is "e".
    ; If "e" is less than the root, the function returns a tree node whose left child is "e",
    ; root is the root of the leaf argument, and whose right child is null, as can be seen from the
    ; code. 
    
    (define (insert-to-left tree e)
      (cond ((null? (left-tree tree)) (list (list '() e '())
                                            (root-of-tree tree)
                                            (right-tree tree)))
            (else (list (tree-based-insert-helper (left-tree tree) e)
                        (root-of-tree tree)
                        (right-tree tree)))
            )
      )
    ;Preconditions: The input must be a list of three elements that represents a tree, "tree",
    ; and a number "e".
    ;Postconditions: This function will return a tree node whose root is the root of "tree",
    ; and whose left structure contains "e".
    ;Role in Program: This function is an auxiliary function to the "tree-based-insert" function.
    ;Proof: Assuming root-of-tree, right-tree, left-tree, and tree-based-insert-helper 
    ; work correctly, this function will work correctly. If the left structure is null, "e" will
    ; become the left structure of the returned tree. Otherwise, tree-based-insert-helper will
    ; be called to insert "e" into the left subtree.
    
    (define (insert-to-right tree e)
      (cond ((null? (right-tree tree)) (list (left-tree tree)
                                             (root-of-tree tree)
                                             (list '() e '())))
            (else (list (left-tree tree)
                        (root-of-tree tree)
                        (tree-based-insert-helper (right-tree tree) e))))
      )
    ;Preconditions: The input must be a list of three elements that represents a tree, "tree",
    ; and a number "e".
    ;Postconditions: This function will return a tree node whose root is the root of "tree",
    ; and whose right structure contains "e".
    ;Role in Program: This function is an auxiliary function to the "tree-based-insert" function.
    ;Proof: Assuming root-of-tree, right-tree, left-tree, and tree-based-insert-helper 
    ; work correctly, this function will work correctly. If the right structure is null, "e" will
    ; become the right structure of the returned tree. Otherwise, tree-based-insert-helper will
    ; be called to insert "e" into the right subtree.
    
    (cond ((null? tset) (list '() e '()))
          ((tree-leaf? tset) (insert-at-leaf tset e))
          ((< e (root-of-tree tset)) (insert-to-left tset e))
          ((= e (root-of-tree tset)) tset)
          ((> e (root-of-tree tset)) (insert-to-right tset e))
          ))
  ;Preconditions: The input must be a list of three items which represents a tree-based set with
  ; the form '(left_structure      root      right_structure), "tset", and an number "e".
  ;Postconditions: This function returns a tree with "e" inserted if "e" is not equal to the
  ; value of the root. 
  ;Role in Program : This function is used to insert an element into a general set if the
  ; underlying data-structure is a tree.
  ;Proof: Assuming tree-leaf?, insert-at-leaf, root-of-tree, insert-to-right, and insert-to-left
  ; work correctly, this function will work correctly. If "e" is equal to the value of the root
  ; of the tree, the function will return tset because sets should not have duplicate elements.
  ; If "e" is less than the value of the root, the element will be inserted into the left tree.
  ; If "e" is greater than the value of the root, the element will be inserted into the right tree.
  
  
  (define (tree-based-remove e) 
    (let ((temp-set (listset)))
      (begin (temp-set 'insert-multiple (tree-to-list))
        (temp-set 'remove e)
        (set! data (list-to-tree (temp-set 'tolist))))))
  ;Preconditions: The input must be a number "e".
  ;Postconditions: After execution, the "data" instance variable now holds a tree that is similar
  ; to the tree that it held at the beginning of execution except that "e" has been removed if it
  ; was in the tree.
  ;Role in Program : This function is used to remove an element from a general set if the
  ; underlying data-structure is a tree.
  ;Proof: Assuming tree-to-list, list-to-tree, letset's remove function, listset's insert-multiple
  ; function, and the listset class itself work correctly, this function will work correctly. 
  ; As can be seen in the code, this function first converts the input set into a list, calls
  ; listset's remove functionon the newly created list to remove "e" form that list, converts the
  ; result back into a tree and finally returns that tree.
  
  (define (tree-based-insert e)
    (set! data (tree-based-insert-helper data e)))
  ;Preconditions: The input must be a number "e".
  ;Postconditions: After execution, the "data" instance variable now holds a tree that is similar
  ; to the tree that it held at the beginning of execution except that "e" has been added if it
  ; was not already in the tree.
  ;Role in Program : This function is used to insert an element into a general set if the
  ; underlying data-structure is a tree.
  ;Proof: Assuming tree-based-insert-helper works correctly, this function will work correctly.
  
  (define (tree-to-list)
    (define (tree-to-list-helper tree)
      (cond ((null? tree) '())
            ((not (pair? tree)) (list tree))
            (else (append (tree-to-list-helper (left-tree tree))
                          (cons (root-of-tree tree)
                                (tree-to-list-helper (right-tree tree)))))))
    ;Preconditions: This function takes a list of three items that represents a tree as input: "tree".
    ;Postconditions: The output for this function is a list that contains the elements that were
    ; stored in "tree" in increasing order.
    ;Role in Program : This is an auxiliary function to tree-to-list
    ;Proof: Induction on depth of tree still to be processed: "k".
    ; Base case - If the input is null or an atom, k is 0 after execution. If it is null, the
    ; function returns null. If it is an atom, the function returns that atom as part of a one
    ; element list.
    ; Wish - This function returns the correct list when given the k-1 case, or a tree of depth
    ; k-1.
    ; Inductive step - This function makes a recursive call on the left and right subtrees separately,
    ; which are both k-1 cases, and combines the list of elements in the left subtree, the root of the
    ; current tree, and the list of elements in the right subtree and returns this.
    
    (tree-to-list-helper data)
    )
  ;Preconditions: This function uses an instance variable as input and does not need any explicit
  ; inputs.
  ;Postconditions: The output for this function is a list that contains the elements that were
  ; stored in the "data" instance variable of the instance that this function was called on.
  ;Role in Program : This function is used to generate a list and sorted list from the members in
  ; a set when the underlying data structure of the set is a tree.
  ;Proof: Assuming tree-to-list-helper works correctly, this function will work correctly.
  
  
  (define (tree-depth)
    (define (tree-depth-helper tree)
      (cond ((null? tree) 0)
            (else (+ 1 (max (tree-depth-helper (left-tree tree))
                            (tree-depth-helper (right-tree tree)))))))
    (tree-depth-helper data))
  ;Preconditions: This function takes an instance variable "data" as its input and does not need
  ; any explicit inputs.
  ;Postconditions: This function returns the depth of the tree "data".
  ;Role in Program: This function is used to give the depth of the "data" of an instance of treeset.
  ;Proof: induct on the depth of the unprocessed tree - "k"
  ; Base case - If the input is null, k must be 0 so 0 is returned
  ; Assume - The function correctly calculates the depth of a tree of depth k-1
  ; Inductive step - If given a tree of depth k, the function adds 1 to the sum of the depths of its
  ; left and right sub-trees. With the current node removed and accounted for, the sum of the elements
  ; in the subtrees is calculated using recursive calls to tree-depth on trees with depths k-1.
  
  
  (let ((parent (set treeset
                     'tree 
                     tree-based-contains 
                     tree-to-list 
                     tree-to-list 
                     tree-based-insert 
                     tree-based-remove
                     tree-size)))
    (lambda (req . args)
      (cond ((eq? req 'depth) (tree-depth))
            (else (apply parent (cons req args)))))))
;Preconditions: This function uses a instance variables and functions as inputs and does not
; need explicit inputs
;Postconditions: The output for this function is an instance of treeset which implements a set
; using a tree as its underlying data structure.
;Usage example: (define tree-name (treeset))
;Proof: Assuming the superclass "set", the instance functions tree-based-contains, tree-to-list,
; tree-based-insert, tree-based-remove and tree-size correctly, this function and the instance it
; returns will work correctly.


;The constructor for list-based set. Takes no arguments, returns a dispatching function that calls
;the generic interface's dispatcher with the arguments passed in.
;This set is backed by a list stored in an instance variable named "data".
;The following functions are implemented: list-based-contains, list-based-insert, list-based-remove
; and list-size.
(define (listset)
  (define data '())
  
  (define (list-size) (length data))
  
  (define (list-based-contains e)
    (define (list-based-contains-helper lset)
      (cond ((null? lset) #f)
            ((= (car lset) e) #t)
            ((> (car lset) e) #f)
            (else (list-based-contains-helper (cdr lset)))))
    (list-based-contains-helper data))
  ;Precondition: Assuming input e is a number.
  ;Postcondition: The output is true if the element is found in the set, and else false
  ;Role in Program: This method is used to help insertion for nonrepeated elements and deletion for
  ; finding the target in the set.
  ;Proof: 
  ;   Base Case: If the lset is null, or if the current element in the set are looking for is greater than 
  ;             the target we are looking for, then return false. and if the current/first elements of the 
  ;             set is the target, then return true;
  ;   Inductive Hypothesis: (list-based-contains e) works for a set of k elements.
  ;   Inductive Step: According to the inductive hypothesis, since (list-base-contains e) works for a set of
  ;             k elements. then it should work for a size of k+1 set. [(list-base-contain e) for set size k+1]
  ;             is similar to [(list-base-contain e) for set size k] AND [(list-base-contain e) for the extra 1].
  ;             So if [(list-base-contain e) for set size k] returns true, then the result of the return would 
  ;             depends on [(list-base-contain e) extra 1]. and if [(list-base-contain e) for set size k] returns
  ;             false, then it does matter what the extra 1 is, it would automatically return false.
  ;Termination: (list-based-contains-helper (cdr lset) deal witht he termination. so each called to 
  ;             (list-based-contains-helper lset), the first element is remove from the list ,and the rest of the list
  ;             is passed into (list-based-contain-helper). and either the current element the list is greater than e
  ;             or the we have a empty list eventually due to the removal of each call to (list-based-contains-helper).
  
  (define (list-based-insert e)
    (define (list-based-insert-helper lset)
      (cond ((null? lset) (list e))
            ( (> (car lset) e) (cons e lset))
            ( (= (car lset) e) lset)
            (else (cons (car lset) (list-based-insert-helper (cdr lset))))))
    (set! data (list-based-insert-helper data)))
  ;Precondition: Assume input e is a valid number input.
  ;POstcondition: we would have a list with the input e correctly inserted.
  ;Role in Program: It is a helper method that helps other methods inserting elements into a list 
  ;Proof:
  ;    base Case: Assume that (list-based-insert e) would return e added to the list, if data is 
  ;               empty.
  ;    Inductive Hypothesis: Assume (list-based-insert e) work for data with size k.
  ;    Inductive step: According to inductive hypothesis, since (list-base-insert e) works correct
  ;               with data of size k, then it should work correctly for data of size k+1. Assuming 
  ;               without  the extra element, e was suppose to be inserted somewhere in the middle of 
  ;               the set, but now with the extra element, we have to consider where the extra element
  ;               belong in, less than e or greater than but far away from e will not effect the
  ;               insertion. the problem is that if the extra element we want to place in is greater 
  ;               than e but less than the number on the right of e. So we should insert e into that
  ;               extra number.
  ;Termination: The termination will happen when inserting fail, which means that the element we are 
  ;             inserting are already in the list. or insertion completed. 
  ;             (else (cons (car lset) (list-based-insert-helper (cdr lset)))) will be responsible to
  ;             loop over the list to find the insertion is completed, if we are not touching anything
  ;             in the list, (cons (car lset)) takes care of it. and eventually we get to a point where
  ;             we complete the insertion when (null? lset) is true or (> (car lset) e) is true. and also
  ;             failing the isnertion, when we have (= (car lset) e).
  
  (define (list-based-remove e)
    (define (list-based-remove-helper lset)
      (cond ((null? lset) '())
            ((= (car lset) e) (cdr lset))
            ((> (car lset) e) lset)
            (else (append (list (car lset)) (list-based-remove-helper (cdr lset))))))
    (set! data (list-based-remove-helper data)))
    ;Precondition: Assume input e is a valid number input.
  ;POstcondition: returns a list that does not contain e.
  ;Role in Program: It is a helper function that helps other methods taking elemnts out of a list.
  ;Proof:
  ;    base Case: Assume that (list-based-remove e) with lset size of 0, and returns the empty list.
  ;    Inductive Hypothesis: Assume (list-based-remove e) works correctly for data with size k.
  ;    Inductive step: According to the inductive hypothesis, since (list-based-remove e) works for
  ;                    lset of size k. then it should work correctly with size k+1. Let lset be the 
  ;                    set of size K, and lset+1 be with the extra element included. THere are tree
  ;                    cases we have to consider, one case is that it is that if set+1 does 
  ;                    not contain e in the set, e are remove in lset, and finally e is the element
  ;                    we are looking for, then just simply remove it and return lset.
  ;Termination: The recursion will termination once deletion is done or element not found in lset or
  ;             we should say we loop to the end of the list. (list-based-remove-helper (cdr lset))
  ;             is responsible to the elements in the list once it is compare, we will eventually 
  ;             stop the recursion when all the elemnts in the list is taken out and return '(). 
  ;             or we found the element we are looking for and terminates.
  
  (set listset
       'list 
       list-based-contains 
       (lambda () data) 
       (lambda () data)
       list-based-insert
       list-based-remove
       list-size))
;Preconditions: This function uses a instance variables and functions as inputs and does not
; need explicit inputs
;Postconditions: The output for this function is an instance of listset which implements a set
; using a list as its underlying data structure.
;Usage example: (define list-name (listset))
;Proof: Assuming the superclass "set", the instance functions list-based-contains, list-based-insert, 
;list-based-remove and list-size correctly, this function and the instance it returns will work correctly.



(define t1 (treeset))
(define t2 (treeset))
(t1 'insert-multiple '(1 2 3 4 5 6))
(t2 'insert-multiple '(1 2 3 4 5 6 7))
(t1 'is-subset-of? t2)

(define l3 (listset))
(l3 'insert-multiple '(2 2 5 6 13 4 6 1 -3 5 26 -4 -35 35 1))
(l3 'tosortedlist)
